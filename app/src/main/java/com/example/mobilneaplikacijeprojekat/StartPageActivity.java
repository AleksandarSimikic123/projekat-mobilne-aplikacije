package com.example.mobilneaplikacijeprojekat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class StartPageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_page);
    }

    public void goToAsocijacije(View view){
        Intent intent = new Intent (this, AsocijacijeActivity.class);
        startActivity(intent);
    }

    public void goToKoZnaZna(View view){
        Intent intent = new Intent (this, KoZnaZnaActivity.class);
        startActivity(intent);
    }

    public void goToMojBroj(View view){
        Intent intent = new Intent (this, MojBrojActivity.class);
        startActivity(intent);
    }

    public void goToSpojnice(View view){
        Intent intent = new Intent(this, SpojniceActivity.class);
        startActivity(intent);
    }

    public void goToKorakPoKorak(View view){
        Intent intent = new Intent (this, KorakPoKorakActivity.class);
        startActivity(intent);
    }
}
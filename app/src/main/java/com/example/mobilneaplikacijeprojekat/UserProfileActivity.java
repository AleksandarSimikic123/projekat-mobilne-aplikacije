package com.example.mobilneaplikacijeprojekat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class UserProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
    }

    public void goToKoZnaZnaStatistikaActivity(View view) {
        Intent intent = new Intent (this, KoZnaZnaStatistikaActivity.class);
        startActivity(intent);
    }

    public void goToMojBrojStatistikaActivity(View view) {
        Intent intent = new Intent (this, MojBrojStatistikaActivity.class);
        startActivity(intent);
    }

    public void goToAsocijacijeStatistikaActivity(View view){
        Intent intent = new Intent (this, AsocijacijeStatistikaActivity.class);
        startActivity(intent);
    }

    public void goToSpojniceStatistikaActivity(View view){
        Intent intent = new Intent(this, SpojniceStatistikaActivity.class);
        startActivity(intent);

    }

    public void goToKorakPoKorakStatistikaActivity(View view){
        Intent intent = new Intent(this, KorakPoKorakStatistikaActivity.class);
        startActivity(intent);
    }

    public void goToSkockoStatistikaActivity(View view){
        Intent intent = new Intent(this, SkockoStatistikaActivity.class);
        startActivity(intent);
    }



}